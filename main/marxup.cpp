#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <list>
#include "../lib/pugixml/pugixml.hpp"
#include "../lib/datatype/datatype.h"
#include "../lib/string-extensions/string-extensions.h"
#include "../lib/debug-extensions/tree.h"

using namespace std;

unordered_map<string, void (*)(pugi::xml_node)> funcs;
unordered_map<string, string> manifestos;

int debug_level = 0; // Default to no debugging

void print2(){
    cout << endl;
}
template<typename T, typename... Args>
void print2(T t, Args... args) // recursive variadic function
{
    if(debug_level >= 2){
        std::cout << t;
        print2(args...);
    }
}

void print1(){
    cout << endl;
}
template<typename T, typename... Args>
void print1(T t, Args... args) // recursive variadic function
{
    if(debug_level >= 1){
        std::cout << t;
        print1(args...);
    }
}

void print0(){
    cout << endl;
}
template<typename T, typename... Args>
void print0(T t, Args... args) // recursive variadic function
{
    if(debug_level >= 0){
        std::cout << t;
        print0(args...);
    }
}

//This is the general version!
void execute(pugi::xml_node node){
    for(auto child : node.children()){
        try{
            funcs.at(child.name())(child);
        }catch(std::out_of_range){
            cout << "I'm sorry, meatbag. I don't know what `" << child.name() << "` means yet. Plox fix me." << endl;
        }
    }
}

//Only use this at the start!
void walk_the_moon(pugi::xml_node node){
    for(auto child : node.children()){
        if( stringextensions::to_lower(child.name()) == "revolution"){
            execute(child);
        }
    }
}

void manifesto(pugi::xml_node node){
    string man_name = "";
    string man_val = "";
    
    print2("This is the manifesto function");
    for(auto child : node.children()){
        print1("`", child.name(), "`");
        
        if(stringextensions::to_lower(child.name()) == "title"){
            man_name = child.child_value();
            print1("Found the title, it's `", man_name, "`");
        }else if(stringextensions::to_lower(child.name()) == "content"){
            man_val = child.child_value();
            print1("content = `", man_val, "`");
        }
        
        print2("ok");
    }
    
    print2("That's my manifesto");
    bool happy = true;
    if(man_name == ""){
        cout << "Bad manifesto title (or none given)!!!" << endl;
        happy = false;
    }
    else if(man_val == ""){
        cout << "Bad manifesto contents (or none given)!!!" << endl;
        happy = false;
    }
    
    if(happy){
        print1("I'm stickin it in! { ", man_name, " : ", man_val, " }");
        manifestos[man_name] = man_val;
    }
    
}

void publish(pugi::xml_node node){
    print2("This is the publish function");
    
    string man_name = "";
    
    for(auto child : node.children()){
        print1("`", child.name(), "`");
        
        if(stringextensions::to_lower(child.name()) == "title"){
            man_name = child.child_value();
            print1("Found the title, it's `", man_name, "`");
        }
        print2("ok");
    }
    
    // We can be sure that's manifestos[""] won't exist because of how we add in manifestos
    try{
        cout << manifestos.at(man_name) << endl;
    }catch(std::out_of_range){
        cout << "I DON'T HAVE ANY MANIFESTOS CALLED `" << man_name << "` YA FOOL!" << endl;
    }
    print1("That's my manifesto");
}


void register_functions(){
    funcs["manifesto"] = manifesto;
    funcs["publish"] = publish;
}

int main(int argc, char **argv){
	
	string fileName;
	
	// This is where we check for an input society and throw errors if users are dumb.
	if(argc != 2){
		cout << "Useage:" << endl;
		cout << argv[0] << " society.mxp" << endl;
		exit(1);
	}else{
		fileName = string(argv[1]);
		cout << "using file: " << fileName << endl;
	}
	
	pugi::xml_document society;
	pugi::xml_parse_result result = society.load_file(argv[1]);
	
	if(!result){
		cout << "There was an issue of some sort processing this file..." << endl;
        cout << "Error: " << result.description() << endl;
        cout << "Error offset: " << result.offset << " (error at [..." << (argv[1] + result.offset) << "]" << endl;
	}
	
	//Find number of 1st level nodes
	size_t first_level = std::distance(society.begin(), society.end());
	if(first_level != 1){
		cerr << "SOCIETY_NOT_UTOPIC: Must be exactly 1 first-level node (found " << first_level << ")" << endl;
		exit(-1);
	}
	pugi::xml_node state = *society.begin();
	
	
	if(stringextensions::to_lower(state.name()) != "state"){
		cerr << "SOCIETY_NOT_UTOPIC: First-level node must be <state> (found <" << state.name() << ">)" << endl;
	}
	
	register_functions();
	
	walk_the_moon(*society.begin());
}
