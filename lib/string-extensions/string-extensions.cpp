#include "string-extensions.h"

void stringextensions::printBits(unsigned char c){
	for(int i=0; i<8; i++){
		cout << ((c & 0b10000000) > 0);
		c <<= 1;
	}
}

bool stringextensions::is_valid_utf8(const string& s){
	
	// Check this ones, save answer. Saves on function calls
	// and we don't modify the string anyway.
	size_t strLen = s.length();
	
	/* Here we assume all strings are valid unless we can prove they don't
		* match the UTF-8 format spec. As soon as any part isn't valud UTF-8
		* we will return false. We also check for not-minimally coded UTF-8
		* per RFC 3629.
		*/
	for(size_t i=0; i < strLen; i++){
		
		//This first one will probably be compiled out.
		if( (s[i] & 0b10000000) == 0){
			//all good!
		}
		
		else if((s[i] & 0b11100000) == 0b11000000){
			// 2-byte codepoint
			
			if( (s[i] & 0b00011111) == 0){
				cout << " (overlong) ";
				return false;
			}
			/*
			else{
				cout << " : " << (s[i] & 0b00011111)<< " : ";
				printBits(s[i]);
				cout << " : ";
				printBits(s[i] & 0b00011111);
				cout << " : ";
			}
			*/
			
			if((i+1 >= strLen) || ((s[i+1] & 0b11000000) != 0b10000000)){
				//No following bit or continuation bits are wrong.
				cout << " (Bitcode) ";
				return false;
			}else{
				i += 1; //skip next byte, valid part of this character.
			}
			
		}else if((s[i] & 0b11110000) == 0b11100000){
			// 3-byte codepoint
			
			if( (s[i] & 0b00001111) == 0){
				cout << " (overlong) ";
				return false;
			}
			
			if(
				(i+2 >= strLen) ||
				((s[i+1] & 0b11000000) != 0b10000000) || 
				((s[i+2]& 0b11000000) != 0b10000000))
			{
				cout << " (bitcode) ";
				return false;
				
			}else{
				i += 2;
			}
			
		}else if((s[i] & 0b11111000) == 0b11110000){
			// 4-byte codepoint
			
			if( (s[i] & 0b00000111) == 0){
				cout << " (overlong) ";
				return false;
			}
			
			if(
				(i+3 >= strLen) ||
				((s[i+1] & 0b11000000) != 0b10000000) ||
				((s[i+2] & 0b11000000) != 0b10000000) ||
				((s[i+3] & 0b11000000) != 0b10000000))
			{
				cout << " (bitcode) ";
				return false;
			}else{
				i += 3;
			}
			
		}
		else{
			cout << " (other) ";
			cout << " (";
			printBits(s[i]);
			cout << ") ";
			return false;
			
		}
	}
	return true;
}

bool stringextensions::is_ascii(char c){
	return (c & 10000000) == 0;
}

string stringextensions::to_lower(const string& s){
	
	string s2 = string(s.length(),' ');
	//s2.reserve(s.length());
	
	if(!is_valid_utf8(s)){
		throw 'a';
	}
	for(size_t i=0; i<s.length(); i++){
		if(is_ascii(s[i])){
			s2[i] = tolower(s[i]);
		}else{
			//Non-ASCII here must still be valid utf-8, so just copy it over.
			s2[i] = s[i];
		}
	}
	
	return s2;
}