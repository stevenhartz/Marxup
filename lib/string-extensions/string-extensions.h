#ifndef STRING_EXTENSIONS_H
#define STRING_EXTENSIONS_H


#include <string>
#include <cctype>

// For debug
#include <iostream>

using namespace std;

namespace stringextensions{
    void printBits(unsigned char c);
	
	bool is_valid_utf8(const string& s);
	
	bool is_ascii(char c);
	
	string to_lower(const string& s);
}

#endif