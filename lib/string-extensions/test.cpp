#include "string-extensions.h"
#include <string>
#include <iostream>

using namespace std;

int main(){
	char ascii_test[] = {'a','b','c',0};
	char twobyte_test[] = {0xc2, 0xa1, 0xc2, 0xab, 0xdf, 0xba,0};
	char thrbyte_test[] = {0xe0, 0xa2, 0xa9, 0xe0, 0xa2, 0xac, 0xe0, 0xae, 0xa3, 0};
	char forbyte_test[] = {0xf0, 0x90, 0xa4, 0x8e, 0};

	string s;
	
	cout << "++++++ Testing valid strings ++++++" << endl;

	s = string(ascii_test);
	cout << s << " is Valid? " << stringextensions::is_valid_utf8(s) << endl;

	s = string(twobyte_test);
	cout << s << " is Valid? " << stringextensions::is_valid_utf8(s) << endl;

	s = string(thrbyte_test);
	cout << s << " is Valid? " << stringextensions::is_valid_utf8(s) << endl;

	s = string(forbyte_test);
	cout << s << " is Valid? " << stringextensions::is_valid_utf8(s) << endl;

	cout << "++++++ Testing invalid strings (1 byte) ++++++" << endl;
	char onebyte_fail_1[] = {0b10000000,0};
	char onebyte_fail_2[] = {0b10000010,0};
	char onebyte_fail_3[] = {0b10001000,0};
	char onebyte_fail_4[] = {0b10100000,0};
	char onebyte_fail_5[] = {0b10111111,0};
	
	s = string(onebyte_fail_1);
	cout << "Test 1: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(onebyte_fail_2);
	cout << "Test 2: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(onebyte_fail_3);
	cout << "Test 3: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(onebyte_fail_4);
	cout << "Test 4: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(onebyte_fail_5);
	cout << "Test 5: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	cout << "++++++ Testing invalid strings (2 bytes) +++++" << endl;
	char twobyte_fail_1[] = {0b11000000, 0b10000000, 0};
	char twobyte_fail_2[] = {0b11000010, 0b01001010, 0};
	char twobyte_fail_3[] = {0b11001000, 0b01000000, 0};
	char twobyte_fail_4[] = {0b11000001, 0b11000000, 0};
	char twobyte_fail_5[] = {0b11000001, 0};
	char twobyte_fail_6[] = {0b11000001, 0b11000010, 0};
	
	s = string(twobyte_fail_1);
	cout << "Test 1: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(twobyte_fail_2);
	cout << "Test 2: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(twobyte_fail_3);
	cout << "Test 3: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(twobyte_fail_4);
	cout << "Test 4: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(twobyte_fail_5);
	cout << "Test 5: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
	s = string(twobyte_fail_6);
	cout << "Test 6: valid? " << stringextensions::is_valid_utf8(s) << endl;
	
}
