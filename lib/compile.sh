#!/bin/bash

flags="-g -c -std=c++11"

for i in $(ls -d */); do
	for x in $( (cd $i && ls *.cpp) ); do
		if [ $x = pugixml.cpp ];then
			echo "(Skipping pugi)"
		elif [ $x != test.cpp ]; then
			echo g++ $flags $i$x -o ${x:0:-3}o
			g++ $flags $i$x -o ${x:0:-3}o
		fi
	done;

done
