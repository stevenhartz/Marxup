#include "datatype.h"
#include "../string-extensions/string-extensions.h"


void printBits(char num){
	for(int i=7; i>=0;i--){
		std::cout << ((num & 0b1<<i)>>i);
	}
	std::cout << std::endl;
}

DataType::DataType(const DataType::Type t){
	long data = 0;
	DataType(t, (void *)&data);
}

DataType::DataType(const DataType::Type t, const void *value){
	
	switch(t){
	case DataType::_int:
		cheapCopy<long>(value);
		type = t;
		break;
	case DataType::_float:
		cheapCopy<double>(value);
		type = t;
		break;
	case DataType::_str:
		strCopy((char *)value);
		type = t;
		break;
	default:
		type = DataType::none;
		break;
	}
}

DataType::DataType(const int value){
	// Force upgrade from int to long on systems where this matters
	long val2 = value;
	type = DataType::_int;
	cheapCopy<long>(&val2);
}

DataType::DataType(const long value){
	type = DataType::_int;
	cheapCopy<long>(&value);
}

DataType::DataType(const float value){
	double val2 = value;
	type = DataType::_float;
	cheapCopy<double>(&val2);
}

DataType::DataType(const double value){
	type = DataType::_float;
	cheapCopy<double>(&value);
}



template<typename T> void DataType::cheapCopy(const void *from){
	dataPtr = malloc(sizeof(T));
	if(dataPtr != 0){
		alloced = true;
		T data = *(T *)from;
		*(T *)dataPtr = data;
	}else{
		throw 'a';
	}
}

void DataType::strCopy(const char *from){
	// Debug
	char pause;
	
	unsigned index = 0;
	// Pass 1: get length of the string
	// Limited to 2^31 for sanity checks.
	unsigned strLen = 0;
	//std::cout << from << std::endl;
	while(from[strLen] != 0 && strLen < 2000000000){
		//std::cout << strLen << std::endl;
		strLen++;
	}
	
	std::cout << "\tLength detected. (" << strLen << ")" << std::endl;
	//std::cin >> pause;
	
	// Checks for valid utf-8
	string s(from);
		if(!stringextensions::is_valid_utf8(s)){
			throw 'a';
		}else{
			//Almost certainly 8, but I'll let the compiler worry about it.
			dataPtr = malloc(sizeof(char) * s.length());
			
			memcpy(dataPtr, from, s.length());
			type = DataType::_str;
		}
		
}

DataType::Type DataType::getType(){
	return DataType::_int;
}

void DataType::castType(DataType::Type t){
	return;
}

DataType DataType::operator+(const DataType &a) const{
	
	switch(type){
		case _int:
			switch(a.type){
				case _int:
					return DataType(*((long *)dataPtr) + *((long *)a.dataPtr));
					break;
				case _float:
					return DataType(*((long *)dataPtr) + *((double *)a.dataPtr));
					break;
				case _str:
					return DataType(0);
					break;
				default:
					throw "Huh??";
			}
			break;
		case _float:
			switch(a.type){
				case _int:
					return DataType(*((double *)dataPtr) + *((long *)a.dataPtr));
					break;
				case _float:
					return DataType(*((double *)dataPtr) + *((double *)a.dataPtr));
					break;
				case _str:
					return DataType(0);
					break;
				default:
					throw "Huh??";
			}
			break;
		default:
			return DataType(0);
	}
}

DataType DataType::concat(const DataType& other) const{
	std::string thisValue, thatValue;
	switch(type){
		case _int:
			thisValue = std::to_string(*((long *)dataPtr));
			break;
		case _float:
			thisValue = std::to_string(*((double *)dataPtr));
			break;
		case _str:
			thisValue = std::string((char *)dataPtr);
	}
	
	switch(other.type){
		case _int:
			thatValue = std::to_string(*((long *)other.dataPtr));
			break;
		case _float:
			thatValue = std::to_string(*((double *)other.dataPtr));
			break;
		case _str:
			thatValue = std::string((char *)other.dataPtr);
	}
	
	return DataType(_str, (thisValue + thatValue).c_str());
}

std::ostream& operator<<(std::ostream& out, const DataType& d){
	
	switch(d.type){
	case DataType::_int:
		return out << "(int)" << *(long *)d.dataPtr;
		break;
	case DataType::_float:
		return out << "(flt)" << *(double *)d.dataPtr;
		break;
	case DataType::_str:
		return out << "(str)" << (char *)d.dataPtr;
		break;
	default:
		return out << "NoneType!!";
		break;
	}
}