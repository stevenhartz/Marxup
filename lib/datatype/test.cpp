#include<iostream>

#include "datatype.h"

using namespace std;


void test_int(){
	long test_ins[] = {0, 1, 4000000000, -1, -4000000000};
	DataType d;
	
	for(int i=0; i<5; i++){
		cout << "Testing input value of: " << test_ins[i] << endl;
		d = DataType(DataType::_int, (void *)&(test_ins[i]));
		cout << "Result: " << d << endl << endl;
	}
}

void test_float(){
	double test_ins[] = {0, 1.0, 321.65245e10, -321.65245e10, 321.65245e-10};
	DataType d;
	
	for(int i=0; i<5; i++){
		cout << "Testing input value of: " << test_ins[i] << endl;
		d = DataType(DataType::_float, (void *)&(test_ins[i]));
		cout << "Result: " << d <<  endl << endl;
	}
}

void test_string(){
	char *s = "Hello!\0Hello, world!";
	char bstring[] = {0b11000110, 0b10010000, 0b11100001, 0b10001000, 0b10001111, 0b00110110, 0xF0, 0xA0, 0x9C, 0x8E, 0};
	char *strings[3];
	strings[0] = &s[0];
	strings[1] = &s[7];
	strings[2] = &bstring[0];
	
	DataType d;
	
	for(int i=0; i<3; i++){
		cout << "Input: " << strings[i] << endl;
		d = DataType(DataType::_str, (void*)strings[i]);
		cout << "Result: " << d << endl << endl;;
	}
}

int main(){
	
	test_int();
	
	test_float();
	
	test_string();
        
        // int-int add
        DataType d1 = DataType(12);
        DataType d2 = DataType(33);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
        d2 = DataType(-10);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
        
        // int-float
        d2 = DataType(3.445);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
        d2 = DataType(-499494.5995);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
        
        //float-int
        d2 = DataType(16.7995);
        cout << d2 << " + " << d1 << endl;
        cout << d2 + d1 << endl;
        d2 = DataType(-4794.5995);
        cout << d2 << " + " << d1 << endl;
        cout << d2 + d1 << endl;
        
        // float-float
        d1 = DataType(3.43);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
        
        d1 = DataType(49.0);
        d2 = DataType(-41.5995);
        cout << d1 << " + " << d2 << endl;
        cout << d1 + d2 << endl;
}