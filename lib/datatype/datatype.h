#include <cstdlib>
#include <string>
#include <iostream>
#include <memory>
#include <cstring>


class DataType{
	
		
	public:
		enum Type {none, _int, _float, _str };
		DataType(const Type = none);
		DataType(const Type, const void*);
                DataType(const int);
                DataType(const long);
                DataType(const float);
                DataType(const double);
		
		Type getType();
		void castType(const Type);
                DataType operator+(const DataType&) const;
                DataType concat(const DataType&) const;
                
		
		friend std::ostream& operator<<(std::ostream& out, const DataType& d);
		
	private:
		Type type;
		void *dataPtr;
		bool alloced;
		template<typename T> void cheapCopy(const void *from);
		void strCopy(const char *from);
};